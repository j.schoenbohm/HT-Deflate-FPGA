module huffman_translate_d_extra_rom_general (
  clka,
  ena,
  addra,
  douta
);

input wire clka;
input wire ena;
input wire [3 : 0] addra;
output reg [15 : 0] douta;
	
// internal memory
reg [15 : 0] memory [0 : 15];

// initialize memory
initial $readmemb("/home/jschoenbohm/HT-Deflate-FPGA/design/kernel/kintex/huffman_translate_d_extra_rom.mif", memory);

always @(posedge clka) begin
	if (ena) begin
		douta <= memory[addra];
	end
end
	
endmodule
