module huffman_translate_dtree_rom_general (
  clka,
  ena,
  addra,
  douta
);

input wire clka;
input wire ena;
input wire [4 : 0] addra;
output reg [7 : 0] douta;
	
// internal memory
//reg [31 : 0] memory [0 : 7];
reg [7 : 0] memory [0 : 31];

// initialize memory
initial $readmemb("/home/jschoenbohm/HT-Deflate-FPGA/design/kernel/kintex/huffman_translate_dtree_rom.mif", memory);

always @(posedge clka) begin
	if (ena) begin
		douta <= memory[addra];
	end
end
	
endmodule
