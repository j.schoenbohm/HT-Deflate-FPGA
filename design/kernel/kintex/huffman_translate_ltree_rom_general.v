module huffman_translate_ltree_rom_general (
  clka,
  ena,
  addra,
  douta
);

input wire clka;
input wire ena;
input wire [8 : 0] addra;
output reg [15 : 0] douta;
	
// internal memory
//reg [511 : 0] memory [0 : 15];
reg [15 : 0] memory [0 : 511];

// initialize memory
initial $readmemb("/home/jschoenbohm/HT-Deflate-FPGA/design/kernel/kintex/huffman_translate_ltree_rom.mif", memory);

always @(posedge clka) begin
	if (ena) begin
		douta <= memory[addra];
	end
end
	
endmodule
