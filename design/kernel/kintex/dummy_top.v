module dummy_top #(
	parameter MEM_BANK_NUM = 'd5,
	parameter MEM_BANK_DEPTH = 'd8
) (
	input logic clk,
	input logic rst,
	input logic start,
	input logic last,
	input logic inValid,
	input logic [255:0] data_window_in,

	output logic [511:0] out_data,
	output logic out_ready,
	output logic out_last,
	output logic [8:0] out_last_len
);

logic [1:0] counter;


always @(posedge clk or posedge rst)
begin
	if (rst)
	begin
		out_data <= 512'b0;
		out_ready <= 1'b0;
		out_last <= 1'b0;
		out_last_len <= 9'b0;
		counter <= 2'b0;
	end
	else if (inValid && start && ~last)
		begin
			out_data <= {384'b0, data_window_in[255:128]};
			out_last_len <= 9'd127;
			counter <= counter + 2'b1;
		end
	else if (inValid && start && last)
		begin
			out_data <= {384'b0, data_window_in[255:128]};
			out_last_len <= 9'd127;
			out_ready <= 1'b1;
			out_last <= 1'b1;
			#2;
			out_ready <= 1'b0;
		end
	else if (inValid && ~start && ~last)
		begin
			out_data <= {out_data[384:0], data_window_in[127:0]};
			counter <= counter + 2'b1;
			out_last_len <= out_last_len + 9'd128;
			out_ready <= (counter == 2'd3);
		end
	else if (inValid && ~start && last)
		begin
			out_data <= {out_data[384:0], data_window_in[127:0]};
			out_last_len <= out_last_len + 9'd128;
			out_last <= 1'b1;
			out_ready <= 1'b1;
			#2;
			out_ready <= 1'b0;
		end
end

endmodule
