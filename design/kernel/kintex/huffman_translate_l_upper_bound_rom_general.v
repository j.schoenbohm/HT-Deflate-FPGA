module huffman_translate_l_upper_bound_rom_general (
  clka,
  ena,
  addra,
  douta
);

input wire clka;
input wire ena;
input wire [2 : 0] addra;
output reg [31 : 0] douta;
	
// internal memory
//reg [7 : 0] memory [0 : 31];
reg [31 : 0] memory [0 : 7];

// initialize memory
initial $readmemb("/home/jschoenbohm/HT-Deflate-FPGA/design/kernel/kintex/huffman_translate_l_upper_bound_rom.mif", memory);

always @(posedge clka) begin
	if (ena) begin
		douta <= memory[addra];
	end
end
	
endmodule
