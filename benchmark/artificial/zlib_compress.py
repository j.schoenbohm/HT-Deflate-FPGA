import zlib
import os

"""
compress_obj = zlib.compressobj(wbits=-15)

in_file_name = "test_128"
orig_file = open(in_file_name, "rb")
out_file_name = "zlib_compressed/" + in_file_name + "_com_zlib"
comp_file = open(out_file_name, "wb")
compressed_output = compress_obj.compress(orig_file.read())
#comp_file.write(compressed_output.decode('utf-8'))
comp_file.write(compressed_output)
orig_file.close()
comp_file.close()
"""

in_file_name = "test_128"
in_file = open("zlib_compressed/" + in_file_name + "_com_zlib", "rb")
#decom_file = open("zlib_compressed/" + in_file_name + "_decom_zlib", "w")
zlib.decompress(in_file.read(), wbits=-15)
in_file.close()
#decom_file.close()
