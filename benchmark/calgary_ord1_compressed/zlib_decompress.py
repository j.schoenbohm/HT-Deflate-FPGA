import zlib
import os

in_file_name = "trans"
in_file = open(in_file_name + "_com1", "rb")
decom_file = open(in_file_name + "_decom1", "wb")
decompressed_output = zlib.decompress(in_file.read(), wbits=-15)
decom_file.write(decompressed_output)
in_file.close()
decom_file.close()
