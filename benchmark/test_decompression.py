import zlib
import os

calgary = "/home/jschoenbohm/HT-Deflate-FPGA/benchmark/calgary"
artificial =  "/home/judith/Documents/HT-Deflate-FPGA/benchmark/artificial"
compress_obj = zlib.compressobj(wbits=-15)
"""
# compress zlib
for entry in os.scandir(calgary):
	in_file_name = "calgary/"+ entry.name
	orig_file = open(in_file_name, "rb")
	out_file_name = "calgary_zlib_compressed/" + entry.name + "_com_zlib"
	comp_file = open(out_file_name, "wb")
	compressed_output = compress_obj.compress(orig_file.read())
	#comp_file.write(compressed_output.decode('utf-8'))
	comp_file.write(compressed_output)
	orig_file.close()
	comp_file.close()
"""
for entry in os.scandir(artificial):
	if entry.is_file():
		print(entry)
		in_file_name = "artificial/"+ entry.name
		orig_file = open(in_file_name, "rb")
		out_file_name = "artificial/zlib_compressed/" + entry.name + "_com_zlib"
		comp_file = open(out_file_name, "wb")
		compressed_output = compress_obj.compress(orig_file.read())
		#comp_file.write(compressed_output.decode('utf-8'))
		comp_file.write(compressed_output)
		orig_file.close()
		comp_file.close()
"""
# decompression
log_file = open("decompression_log", "w")

artificial = "/home/jschoenbohm/HT-Deflate-FPGA/benchmark/artificial"
benchmark = "/home/jschoenbohm/HT-Deflate-FPGA/benchmark"
for i in [1, 2]:
	log_file.write("\n--- ordering "+ str(i) + ": artificial files ---\n\n")
	directory_art = artificial + "/ord" + str(i) + "_compressed"
	for entry in os.scandir(directory_art):
		if entry.is_file():
			in_file = open(directory_art + "/" + entry.name, "rb")
			#decom_file = open("calgary/obj1_decompressed", "w")
			try:
				#decom_file.write(zlib.decompress(in_file.read(), wbits=-15))
				zlib.decompress(in_file.read(), wbits=-15)
			except BaseException as e:
				log_file.write(entry.name + ":\t\t" + str(e)+"\n")
			in_file.close()
			#decom_file.close()

	log_file.write("\n--- ordering "+ str(i) + ": calgary files ---\n\n")
	directory_cal = benchmark + "/calgary_ord"+str(i)+"_compressed"
	for entry in os.scandir(directory_cal):
		if entry.is_file():
			in_file = open(directory_cal + "/" + entry.name, "rb")
			#decom_file = open("calgary/obj1_decompressed", "w")
			try:
				#decom_file.write(zlib.decompress(in_file.read(), wbits=-15))
				zlib.decompress(in_file.read(), wbits=-15)
			except BaseException as e:
				log_file.write(entry.name + ":\t\t" + str(e) + "\n")
			in_file.close()
			#decom_file.close()
"""
